`chemfeat <https://gitlab.inria.fr/jrye/chemfeat>`_ Documentation
=================================================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    readme
    gen_command_help
    gen_feature_set_configuration
    gen_features
    modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
